# OpenML dataset: stock-market-prediction

https://www.openml.org/d/43645

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
this dataset is containing information about HDFC bank equity share information.
Content
The dataset containing a total of 7 columns  Date         5151 non-null datetime64[ns]
Open         5082 non-null float64
High         5082 non-null float64
Low          5082 non-null float64
Close        5082 non-null float64
Adj Close    5082 non-null float64
Volume  5082 non-null float64

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43645) of an [OpenML dataset](https://www.openml.org/d/43645). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43645/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43645/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43645/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

